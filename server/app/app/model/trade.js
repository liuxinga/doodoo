const user = require("./user");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "trade",
    hasTimestamps: true,
    user: function() {
        return this.belongsTo(user, "user_id");
    }
});
