const util = require("./../../utils/util.js");

Component({
    options: {
        multipleSlots: false // 在组件定义时的选项中启用多slot支持
    },
    properties: {
        props: String
    },
    data: {
        imgs: [{
            imgUrl: "",
            targetType: "",
            targetUrl: ""
        },
        {
            imgUrl: "",
            targetType: "",
            targetUrl: ""
        },
        {
            imgUrl: "",
            targetType: "",
            targetUrl: ""
        }
        ],
        autoplay: true,
        current: 0,
        interval: 5000,
        duration: 1000,
        circular: true,
        indicatorShow: true,
        indicatorType: 0,
        indicatorPosition: 'center',
        indicatorColor: 'rgba(255, 255, 255, 0.6)',
        indicatorActiveColor: '#ffffff',
        width: '100%',
        height: 210
    },
    attached() {
        this.setData(
            Object.assign(
                this.data,
                this.data.props ? JSON.parse(this.data.props) : {}
            )
        );
        this.setData({
            height: util.px2rpx(this.data.height)
        })
    },
    methods: {
        //自动切换事件
        changeSlide: function (e) {
            this.setData({
                current: e.detail.current
            });
        },

        formSubmit(e) {
            console.log('form发生了submit事件，携带数据为：', e.detail)
            wx.doodoo.fetch("/form/api/formid/add", {
                method: "post",
                data: {
                    form_id: e.detail.target.dataset.formid,
                    formId: e.detail.formId
                }
            });
        }
    }
})