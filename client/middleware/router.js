export default function ({ app, route, redirect, req }) {
    if (route.path === "/admin") {
        redirect("/admin/public/login");
        return
    }
    if (route.path === "/" || route.matched.length === 0) {
        redirect("/app/apps");
        return
    }
}
